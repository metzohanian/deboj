#!/usr/bin/env php
<?php

//require_once('../../autoload.php');

/*

  Commands
    create <server> <application> <namespace> [<directory>]
    library <library-name>
*/

function clear($directory = null) {
  $directory = is_null($directory) ? preg_replace('/\/vendor\/metzohanian\/deboj\/bin$/', '', __DIR__) : $directory;
  @deleteDirectory($directory . "/lib/driver");
  deleteDirectory($directory . "/lib");
  deleteDirectory($directory . "/model");
  deleteDirectory($directory . "/language");
  deleteDirectory($directory . "/config");
  deleteDirectory($directory . "/log");
  @unlink($directory . "/index.php");
  @unlink($directory . "/.env");
  echo "\n\t" . EColors::fg('red', "Application directory cleared.") . "\n\n";
}

function deleteDirectory($dir) {
  system('rm -rf ' . escapeshellarg($dir), $retval);
  return $retval == 0; // UNIX commands return zero on success
}

function create($server, $application, $namespace, $dest = null) {
  if ($namespace[0] != "\\" || $namespace[strlen($namespace)-1] != "\\") {
    echo "\nNamespace must start and end with \\\n";
    throw new \Exception("Namespace must start and end with \\");
  }
  
  $dest = is_null($dest) ? preg_replace('/\/vendor\/metzohanian\/deboj\/bin$/', '', __DIR__) : $dest;
  $skel = rtrim(__DIR__, "bin") . "skel";
  if (!file_exists($dest)) {
    echo EColors::fg('red', "Destination directory does not exist: ") . EColors::fg('bold_red', $dest) . "\n";
    return false;
  }
  if (!file_exists($skel)) {
    echo EColors::fg('red', "Source skeleton directory does not exist: ") . EColors::fg('bold_red', $skel) . "\n";
    return false;
  }
  copyr($skel, $dest);
  system("egrep -lRZ '{{NAMESPACE}}' $dest/lib | xargs -0 -l sed -i -e 's/{{NAMESPACE}}/" . addslashes(trim($namespace, "\\")) . "/g'");
  system("egrep -lRZ '{{NAMESPACE}}' $dest/model | xargs -0 -l sed -i -e 's/{{NAMESPACE}}/" . addslashes(trim($namespace, "\\")) . "/g'");
  mkdir($dest . '/language');
  mkdir($dest . '/log');
  create_env($server, $application, $namespace, $dest);
  echo "\n\t" . EColors::fg('green', "Application $application configured in $dest.") . "\n\n";
}

set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

if (count($argv) > 1 && function_exists($argv[1])) {
  try {
    call_user_func_array($argv[1], array_slice($argv, 2));
  } catch (Exception $e) {
    print_basic_help();
  }
} else {
  print_basic_help();
}

function print_basic_help() {
    echo 
"\n" . EColors::fg('green', "deboj v0.0.1") . "\n" .
"  " . EColors::fg('yellow', "Commands") . "\n" .
"    ". EColors::fg('white', 'create') . " <server> <application> <namespace> [<directory>]\n".
"    ". EColors::fg('white', 'library') . " <library-name>\n".
"\n";
}

function library($name) {
  $dotenv = new \Dotenv\Dotenv('../../../');
  $dotenv->load();
  file_put_contents( "../../../lib/$name.php",
"<?php
namespace " . getenv('APP_NAMESPACE') . ";

class " . $name . " {

  public function __construct() {
    
  }
  
}");
  echo "\n\t" . EColors::fg('green', "Library $name created.") . "\n\n";
}

function create_env($server, $application, $namespace, $directory = null, $driver = null, $hostname = null, $username = null, $password = null, $database = null) {
  file_put_contents( $directory . "/.env", '
  # Application & Server Configuration
  DEFAULT_TIMEZONE="UTC"
  SERVER="' . $server . '"
  APPLICATION="' . $application . '"
  APPROOT="' . realpath($directory) . '"
  SYSTEM="${APPROOT}/vendor/metzohanian/deboj/src"
  APPLIB="${APPROOT}/lib"
  APP_NAMESPACE="' . str_replace("\\", "\\\\", $namespace) . '"
  BOOTSTRAP = "config/bootstrap"
  APPCONFIG = "config/appconfig"

  # Database
  DB_DRIVER="' . $driver . '"
  DB_HOSTNAME="' . $hostname . '"
  DB_USERNAME="' . $username . '"
  DB_PASSWORD="' . $password . '"
  DB_DATABASE="' . $database . '"
  DBPRE=""

  # Error Handling
  ERROR_REPORTING="E_ALL"
  DISPLAY_ERRORS="on"
  ');
}

/**
 * Color escapes for bash output
 */
class EColors
{
 private static $foreground = array(
  'black' => '0;30',
  'dark_gray' => '1;30',
  'red' => '0;31',
  'bold_red' => '1;31',
  'green' => '0;32',
  'bold_green' => '1;32',
  'brown' => '0;33',
  'yellow' => '1;33',
  'blue' => '0;34',
  'bold_blue' => '1;34',
  'purple' => '0;35',
  'bold_purple' => '1;35',
  'cyan' => '0;36',
  'bold_cyan' => '1;36',
  'white' => '1;37',
  'bold_gray' => '0;37',
 );
 
 private static $background = array(
  'black' => '40',
  'red' => '41',
  'magenta' => '45',
  'yellow' => '43',
  'green' => '42',
  'blue' => '44',
  'cyan' => '46',
  'light_gray' => '47',
 );
 
  
 /**************************************************
 
   http://www.ingeniousmalarkey.com/2011/02/add-color-to-php-echo-in-cli.html
   
   Thanks!
   
 **************************************************/
 /**
  * Make string appear in color
  */
 public static function fg($color, $string)
 {
  if (!isset(self::$foreground[$color]))
  {
   throw new Exception('Foreground color is not defined');
  }
 
  return "\033[" . self::$foreground[$color] . "m" . $string . "\033[0m";
 }
 
 /**
  * Make string appear with background color
  */
 public static function bg($color, $string)
 {
  if (!isset(self::$background[$color]))
  {
   throw new Exception('Background color is not defined');
  }
 
  return "\033[" . self::$background[$color] . 'm' . $string . "\033[0m";
 }
 
}

/**
 * Copy a file, or recursively copy a folder and its contents
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @return      bool     Returns TRUE on success, FALSE on failure
 */
function copyr($source, $dest)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }
    
    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        copyr("$source/$entry", "$dest/$entry");
    }

    // Clean up
    $dir->close();
    return true;
}