<?php
namespace MetzOhanian\Deboj;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

// Create the logger
$logger = new Logger('Application');
// Now add some handlers
$logger->pushHandler(new RotatingFileHandler(getenv('APPROOT') . '/log/debug', 7, Logger::DEBUG));
$logger->pushHandler(new RotatingFileHandler(getenv('APPROOT') . '/log/info', 7, Logger::INFO));
$logger->pushHandler(new RotatingFileHandler(getenv('APPROOT') . '/log/warning', 7, Logger::WARNING));
$logger->pushHandler(new RotatingFileHandler(getenv('APPROOT') . '/log/error', 7, Logger::ERROR));

// You can now use your logger
$logger->info('Default Logger Initialized.');
$this->setLogger($logger);

$this->setPostSetup(function() {});
$this->setCors(function() {});
