<?php
namespace MetzOhanian\Deboj;

$this->setRepository(function() {
    $yapo_errmode = isset(Lib::$Config->YAPO_ERRMODE) ? Lib::$Config->YAPO_ERRMODE : PDO::ERRMODE_SILENT;
    return new \MetzOhanian\Yapo\Driver\Database\Mysql(getenv('DB_HOSTNAME'), getenv('DB_DATABASE'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), $yapo_errmode);  
});

$this->setProcessor(new JsonRpcServer(), function($J) {
  $J->process();
});

$this->preInvoke(function($class, $method, $methodParameters, $callParameters) {
  return Lib::$Lib->Aop->PreCall($class, $method, $methodParameters, $callParameters);
});

$this->postInvoke(function($class, $method, $methodParameters, $callParameters, $result) {
  return Lib::$Lib->Aop->PostCall($class, $method, $methodParameters, $callParameters, $result);
});