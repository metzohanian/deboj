<?php

namespace {{NAMESPACE}};

class User {

  public function __construct() {
    $this->User = & \MetzOhanian\Deboj\Lib::$Model->User;
  }
  
  public function Register($email) {
    return $this->User->Register($email);
  }
  
  public function Authenticate($email, $password) {
    return $this->User->Authenticate($email, $password);
  }
}