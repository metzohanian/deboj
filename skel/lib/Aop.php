<?php

namespace {{NAMESPACE}};

class Aop {

  public function __construct() {
    
  }
  
  public function PreCall($class, $method, $methodParameters, $callParameters) {
    return array($class, $method, $callParameters);
  }
  
  public static function ToObject($result) {
    if (is_a($result, "\\MetzOhanian\\Yapo\\Yapo") || 
        is_a($result, "\\MetzOhanian\\Yapo\\Audit") ||
        is_a($result, "\\MetzOhanian\\Yapo\\ResultSet") ||
        is_a($result, "\\MetzOhanian\\Yapo\\AuditEx")) {
      return $result->toObject();
    }
    return $result;
  }
  
  public static function ConvertOutput($r) {
    $camelResult = new \stdClass();

    foreach ($r as $key => $value) {
      $cKey = lcfirst(str_replace('_', '', ucwords($key, '_')));
      $camelResult->$cKey = $value;
    }
    
    return $camelResult;
  }
  
  public function PostCall($class, $method, $methodParameters, $callParameters, $result) {
    $prune = [ "password_hash", "client_secret", "api_hash" ];
    
    if (is_a($result, "\\MetzOhanian\\Yapo\\Yapo") || 
        is_a($result, "\\MetzOhanian\\Yapo\\Audit") ||
        is_a($result, "\\MetzOhanian\\Yapo\\ResultSet") ||
        is_a($result, "\\MetzOhanian\\Yapo\\AuditEx")) {
      
      $r = $result->toObject();
      
      foreach ($prune as $p => $remove)
        if (isset($r->$remove))
          unset($r->$remove);

      $camelResult = \{{NAMESPACE}}\Aop::ConvertOutput($r);
      return $camelResult; 
    }
    
    return $result;
  }
 
}
