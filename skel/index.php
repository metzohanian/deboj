<?php
namespace MetzOhanian\Deboj;

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("vendor/autoload.php");
$dotenv = new \Dotenv\Dotenv(__DIR__);
$dotenv->load();

$app = new Application(new Logger());
$app->run();

?>