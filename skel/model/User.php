<?php
namespace {{NAMESPACE}}\Model;
use \MetzOhanian\Yapo as Yapo;

class User {

  public function __construct() {
  }
  
  public function Register($email) {
    return $this->driver->Register($email);
  }
  
  public function Authenticate($email, $password) {
    return $this->driver->Authenticate($email, $password);
  }
}