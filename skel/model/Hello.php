<?php
namespace {{NAMESPACE}}\Model;

class Hello {

  public function __construct() {
    
  }
  
  public function World($world = "World") {
    $world = ucfirst($world);
    return "Hello, $world."; 
  }
}