<?php
namespace {{NAMESPACE}}\Model\Driver;
use \MetzOhanian\Yapo as Yapo;

class User {

  public function __construct() {
    $this->users = new Yapo\Yapo(\MetzOhanian\Deboj\Lib::$Sys->Repository, 'users');
  }
  
  public function Register($email) {
    $this->users->clear();
    $this->users->email = $email;
    if ($this->users->find() == 1) {
      return 0;
    }
  }
  
  public function Authenticate($email, $password) {
    $this->users->clear();
    $this->users->email = $email;
    if ($this->users->find() == 1 && password_verify($password, $this->users->password_hash)) {
      return array( 'uuid' => Yapo\Components\Guid::toCanonical($this->users->user_uuid) ); 
    }
    return array(password_hash($password, PASSWORD_DEFAULT), Yapo\Components\Guid::toCanonical(Yapo\Components\Guid::hotSpot()));
  }
}
