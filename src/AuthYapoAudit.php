<?php
namespace MetzOhanian\Deboj;

class AuthYapoAudit extends MetzOhanian\Yapo\Structures\YapoAudit {

	function __construct(& $database = null, $table = null) {
		Lib::$Sys->Requires->Provided('AuthYapoAudit');
	
		if (is_null($database))
			return;
			
		parent::__construct($database, $table, $table . '_history', array( 'modified_by_id', 'modified_at' ));
	}
	
	public function save($all = false) {
		$User = Lib::$Sys->Session->User;
		$this->modified_by_id = $User->UserId;

		$this->modified_at = date("Y-m-d H:i:s");
		$pk = $this->primarykey();
		$fields = $this->__Core->__field_values;
		$this->__HISTORY_YAPO->fields = json_encode(array_keys(array_pop_val($fields, $pk)));
		return parent::save();
	}
}

?>