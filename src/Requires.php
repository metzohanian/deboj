<?php
namespace MetzOhanian\Deboj;

class Requires {

	static $__PROVIDED;
	static $__PROVIDES;

	function __construct() {
		if (!isset(Requires::$__PROVIDED))
			Requires::$__PROVIDED = array();
		if (!isset(Requires::$__PROVIDES))
			Requires::$__PROVIDES = array();
	}
	
	/**************************
	
		A Promise is a gated feature -- 
			it indicates that a type of service is available
		A Contract is a callable feature -- 
			it indicates that an addressable worker is available as a feature
	
	**************************/
	function IsPromised($requirement) {
		return in_array($requirement, Requires::$__PROVIDED);
	}
	
	function HasContract($requirement) {
		return in_array($requirement, Requires::$__PROVIDES);
	}
	
	/**************************

		Promise asserts that this feature service 
			is loaded and ready for operation
		Contract signifies the service is available and provides 
			an interface for accessing that service procedurally
	
	**************************/
	function Promise($requirement) {
		Requires::$__PROVIDED[] = $requirement;
	}
	
	function Contract($requirement, $call) {
		Requires::$__PROVIDES[] = $call;
	}
	
	/**************************

		The callable interface for a Contract
	
	**************************/
	function __call($requirement, $args) {
		if (in_array($requirement, Requires::$__PROVIDES)) {
			return call_user_func_array(Requires::$__PROVIDES[$requirement], $args);
		}
		return false;
	}
}

?>