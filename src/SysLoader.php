<?php

namespace MetzOhanian\Deboj;

class SysLoader
{
  private $DIR;
  private $NAMESPACE;
  
  public static function force_include($include)
  {
    if (!file_exists($include)) 
      throw new \Exception("Required file " . $include . " could not be found.");
    include_once ($include);
  }

  public function __construct()
  {
    $this->DIR = getenv('SYSTEM');
    $this->NAMESPACE = "\\MetzOhanian\\Deboj";
  }

  public static function invokeMethod($class, $method, $arguments) {
    
  }
  
  public function __get($class)
  {
    $classname = $this->NAMESPACE . "\\$class";
    $classfile = "$class.php";
    SysLoader::force_include($this->DIR . '/' . $classfile);
    if (class_exists($classname)) {
      $this->$class = new $classname();
    }
    else {
      throw new \Exception("Required system file " . $this->DIR . '/' . "$classfile could not be found, or class $classname could not instantiated.");
    }

    return $this->$class;
  }

}

