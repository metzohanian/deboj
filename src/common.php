<?php

function detectFileMimeType($filename='')
{
    $filename = escapeshellcmd($filename);
    $command = "file -b --mime-type -m /usr/share/misc/magic {$filename}";

    $mimeType = shell_exec($command);
            
    return trim($mimeType);
}

function array_pop_val(& $arr, $val) {
	unset($arr[$val]);
	return $arr;
}

function is_closure($t) {
	return is_object($t) && ($t instanceof Closure);
}
	
function valid_id($id) {
    return is_numeric($id) && $id > 0;
}

function trimlen($str) {
	return strlen(trim($str));
}

function pad_array($r, $count) {
	return array_merge(array_fill(0, $count, null), $r);
}

function strip_tags_r($val) {
    return is_array($val) ?
    	array_map('strip_tags_r', $val) :
		strip_tags($val);
}

function path_join() {
    $args = func_get_args();
    $paths = array();
    foreach ($args as $arg) {
        $paths = array_merge($paths, (array)$arg);
    }

    $paths = array_map(create_function('$p', 'return trim($p, "/");'), $paths);
    $paths = array_filter($paths);
    return join('/', $paths);
}

function truemod($num, $mod) {
	return ($mod + ($num % $mod)) % $mod;
}

function strip_safe_json_encode($data) {
	return str_replace('\\u0000', "", json_encode($data, !JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE));
}

function array_pluck ($toPluck, $arr) {
    return array_map(function ($item) use ($toPluck) {
        return $item[$toPluck];
    }, $arr); 
}

function is_assoc(array $array) {
  return (bool)count(array_filter(array_keys($array), 'is_string'));
}

function mb_strcasecmp($str1, $str2, $encoding = null) {
    if (null === $encoding) { $encoding = mb_internal_encoding(); }
    return strcmp(mb_strtoupper($str1, $encoding), mb_strtoupper($str2, $encoding)) == 0;
}

function is_json($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}

function get_overridden_methods($class)
{
    $rClass = new ReflectionClass($class);
    $array = NULL;
        
	if ($rClass->getParentClass() === false) {
		return get_class_methods($class);
	}
    foreach ($rClass->getMethods() as $rMethod)
    {
        try
        {
            // attempt to find method in parent class
            new ReflectionMethod($rClass->getParentClass()->getName(), $rMethod->getName());
            // check whether method is explicitly defined in this class
            if ($rMethod->getDeclaringClass()->getName() == $rClass->getName())
            {
                // if so, then it is overriden, so add to array
                $array[] .=  $rMethod->getName();
            }
        }
        catch (exception $e)
        {    $array[] .=  $rMethod->getName();    }
    }
    
    return $array;
}

if (!function_exists('is_countable')) {
    function is_countable($var) {
        return (is_array($var) || $var instanceof Countable);
    }
}
