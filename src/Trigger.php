<?php
namespace MetzOhanian\Deboj;

class Trigger {

	static $Events;
	static $Intercepts;
	static $InterceptOnce;
	
	public function __construct() {
		Trigger::$Events = array();
		Trigger::$Intercepts = array();
		Trigger::$InterceptOnce = array();
	}
	
	public static function Watch($Trigger, $Call) {
		if (!isset(Trigger::$Events[$Trigger]) || !is_array(Trigger::$Events[$Trigger]))
			Trigger::$Events[$Trigger] = array();
		Trigger::$Events[$Trigger][] = $Call;
	}
	
	public static function Trigger($Trigger) {
		if (isset(Trigger::$Events[$Trigger])) foreach (Trigger::$Events[$Trigger] as $t => $Call) {
			call_user_func_array($Call, array_slice(func_get_args(), 1));
		}
	}
	
	public static function EventIntercept($Intercept, $Call, $Once = false) {
		Trigger::$Intercepts[$Intercept] = $Call;
		Trigger::$InterceptOnce[$Intercept] = $Once;
	}
	
	public static function IsIntercepted($Intercept) {
		return isset(Trigger::$Intercepts[$Intercept]);
	}
	
	public static function CallIntercept($Intercept) {
		if (isset(Trigger::$Intercepts[$Intercept])) {
			$Call = Trigger::$Intercepts[$Intercept];
			unset(Trigger::$Intercepts[$Intercept]);
			$callreturn = call_user_func_array($Call, array_slice(func_get_args(), 1));
			if (!Trigger::$InterceptOnce[$Intercept])
				Trigger::EventIntercept($Intercept, $Call);
			return $callreturn;
		}
		return false;
	}
	
}

?>