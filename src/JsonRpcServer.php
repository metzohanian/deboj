<?php
namespace MetzOhanian\Deboj;

/**
 * 
 
 https://github.com/EvilScott/junior
 https://github.com/mojtabacazi/CSJRPC
 Modified July 6, 2015 by Noah Smith (c) MetzOhanian
 
 Copyright (c) 2011 Vertive, Inc.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 **/

/* <Some Constants> */
const JSON_RPC_VERSION = "2.0";
const VALID_FUNCTION_NAME = '/^[a-zA-Z_][a-zA-Z0-9_]*$/';
/* </Some Constants> */

/* <Error Codes> */
const ERROR_MISMATCHED_VERSION = -32000;
const ERROR_RESERVED_PREFIX = -32001;
const ERROR_EXCEPTION = -32099;
const ERROR_INVALID_REQUEST = -32600;
const ERROR_METHOD_NOT_FOUND = -32601;
const ERROR_INVALID_PARAMS = -32602;
const ERROR_PARSE_ERROR = -32700;
/* </Error Codes> */


class JsonRpcServer {
  
  public $input, $decomposed;
  public $available_classes;
  
  // create new server
  public function __construct() {
    
    $this->MODEL   = array();
    $this->LANGUAGE   = array();
    
    $this->available_classes = array_merge(Lib::$Config->STD_ENTITY, Lib::$Config->OTHER_ENDPOINTS);
    Application::$Logger->info("RPC available classes.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, $this->available_classes));
    
    $this->input = 'php://input';
  }
  
  // process json-rpc request
  public function process() {
    // try to read input
    try {
      $json = file_get_contents($this->input);
      Application::$Logger->info("Protocol input (JSON)", array(__FILE__, __LINE__, __CLASS__, __METHOD__, $json));
    }
    catch (Exception $e) {
      $message = "Server unable to read request body.";
      $message .= PHP_EOL . $e->getMessage();
      throw new Exception($message);
    }
    
    // handle communication errors
    if ($json === false) {
      throw new Exception("Server unable to read request body.");
    }
    
    // create request object
    $request = $this->makeRequest($json);
    
    // set content type to json if not testing
    if (!(defined('ENV') && ENV == 'TEST')) {
      header('Content-type: application/json');
    }
    
    // handle json parse error and empty batch
    if ($request->error_code && $request->error_message) {
      echo $request->toResponseJSON();
      return;
    }
    
    // respond with json
    echo $this->handleRequest($request);
  }
  
  // create new request (used for test mocking purposes)
  public function makeRequest($json) {
    return new JsonRpcRequest($json);
  }
  
  // handle request object / return response json
  public function handleRequest($request) {
    // recursion for batch
    if ($request->isBatch()) {
      $batch = array();
      foreach ($request->requests as $req) {
        $batch[] = $this->handleRequest($req);
      }
      $responses = implode(',', array_filter($batch, function($a) {
        return $a !== null;
      }));
      if ($responses != null) {
        return "[{$responses}]";
      } else {
        return null;
      }
    }
    
    // check validity of request
    if ($request->checkValid()) {
      // check for method existence
      if (($method = Application::methodIsCallable($request->caller, $request->method)) === false) {
        $request->error_code    = ERROR_METHOD_NOT_FOUND;
        $request->error_message = "Method not found: " . getenv('APP_NAMESPACE') . $request->caller . "::" . $request->method;
        return $request->toResponseJSON();
      }
      
      // try to call method with params
      try {
        // check if this endpoint is available to RPC
        if (!in_array($request->caller, $this->available_classes))
          throw new \Exception("This endpoint is not available.");
        
        $calling_class = $request->caller;

        $response = Application::invokeMethod($request->caller, $request->method, $request->params);
        if (!$request->isNotify()) {
          $request->result = $response;
        } else {
          return null;
        }
        // handle exceptions
      }
      catch (\Exception $e) {
        if (is_callable(Application::$PostInvokeError)) {
          call_user_func(Application::$PostInvokeError, $request->caller, $request->method, $request->params, $e);
        }
        Lib::$Log->critical("An unhandled exception was thrown during API method invocation.", array($e, func_get_args()));
        $request->error_code    = $e->getCode() < 0 ? $e->getCode() : ERROR_EXCEPTION;
        $request->error_message = $e->getMessage();
      }
    }
    
    // return whatever we got
    return $request->toResponseJSON();
  }
}
