<?php
namespace MetzOhanian\Deboj;

/**
 * 
 
 https://github.com/EvilScott/junior
 https://github.com/mojtabacazi/CSJRPC
 Modified July 6, 2015 by Noah Smith (c) MetzOhanian
 
 Copyright (c) 2011 Vertive, Inc.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 **/

class JsonRpcRequest {
  public $batch;
  public $raw;
  public $result;
  public $json_rpc;
  public $error_code;
  public $error_message;
  public $method;
  public $params;
  public $id;
  
  // create new server request object from raw json
  public function __construct($json) {
    $this->batch = false;
    $this->raw   = $json;
    
    // handle empty request
    if ($this->raw === "") {
      $this->json_rpc      = JSON_RPC_VERSION;
      $this->error_code    = ERROR_INVALID_REQUEST;
      $this->error_message = "Invalid Request.  The message body contains no payload or an empty request was received.";
      return;
    }
    
    // parse json into object
    $obj = json_decode($json);
    
    // handle json parse error
    if ($obj === null) {
      $this->json_rpc      = JSON_RPC_VERSION;
      $this->error_code    = ERROR_PARSE_ERROR;
      $this->error_message = "Parse error.";
      return;
    }
    
    // array of objects for batch
    if (is_array($obj)) {
      
      // empty batch
      if (count($obj) == 0) {
        $this->json_rpc      = JSON_RPC_VERSION;
        $this->error_code    = ERROR_INVALID_REQUEST;
        $this->error_message = "Invalid Request.  The message body could not be parsed.";
        return;
      }
      
      // non-empty batch
      $this->batch    = true;
      $this->requests = array();
      foreach ($obj as $req) {
        // recursion for bad requests
        if (!is_object($req)) {
          $this->requests[] = new JsonRpcRequest('');
          // recursion for good requests
        } else {
          $this->requests[] = new JsonRpcRequest(json_encode($req));
        }
      }
      
      // single request
    } else {
      $this->json_rpc = $obj->jsonrpc;
      if (count(explode('/', $obj->method)) > 1) {
        $parts        = explode('/', $obj->method);
        $this->caller = $parts[0];
        $this->method = $parts[1];
      } else {
        $this->caller = $obj->method;
        $this->method = null;
      }
      if (property_exists($obj, 'params')) {
        $this->params = json_decode(json_encode($obj->params), true);
      }
      ;
      if (property_exists($obj, 'id')) {
        $this->id = $obj->id;
      }
      ;
    }
  }
  
  // returns true if request is valid or returns false assigns error
  public function checkValid() {
    // error code/message already set
    if ($this->error_code && $this->error_message) {
      return false;
    }
    
    // missing jsonrpc or method
    if (!$this->json_rpc || !$this->method) {
      $this->error_code    = ERROR_INVALID_REQUEST;
      $this->error_message = "Invalid Request.  There is no jsonrpc call or no method exists.";
      return false;
    }
    
    // reserved method prefix
    if (substr($this->method, 0, 4) == 'rpc.') {
      $this->error_code    = ERROR_RESERVED_PREFIX;
      $this->error_message = "Illegal method name; Method cannot start with 'rpc.'";
      return false;
    }
    
    // illegal method name
    if (!preg_match(VALID_FUNCTION_NAME, $this->method)) {
      $this->error_code    = ERROR_INVALID_REQUEST;
      $this->error_message = "Invalid Request.  No method could be matched.";
      return false;
    }
    
    // mismatched json-rpc version
    if ($this->json_rpc != "2.0") {
      $this->error_code    = ERROR_MISMATCHED_VERSION;
      $this->error_message = "Client/Server JSON-RPC version mismatch; Expected '2.0'";
      return false;
    }
    
    // valid request
    return true;
  }
  
  // returns true if request is a batch
  public function isBatch() {
    return $this->batch;
  }
  
  // returns true if request is a notification
  public function isNotify() {
    if ($this->id) {
      return false;
    }
    return true;
  }
  
  // return raw JSON response
  public function toResponseJSON() {
    // successful response
    $arr = array(
      'jsonrpc' => JSON_RPC_VERSION
    );
    if ($this->result !== null) {
      $arr['result'] = $this->result;
      $arr['id']     = $this->id;
      return json_encode($arr);
      // error response
    } else {
      $arr['error'] = array(
        'code' => $this->error_code,
        'message' => $this->error_message,
        'data' => isset($this->data) ? $this->data : null
      );
      $arr['id']    = $this->id;
      return json_encode($arr);
    }
  }
}