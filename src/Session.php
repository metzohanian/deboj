<?php
namespace MetzOhanian\Deboj;

class Session {
  function __construct($private_key = null) {
    session_set_cookie_params(60 * 60, "/");
    session_start();
    $timeout = strtotime(date("Y-m-d H:i:s", time() + 60 * 60 * 24 * 61));
    setcookie(session_name(), session_id(), $timeout, "/");
    if (!isset($_SESSION['Session_Vars'])) $_SESSION['Session_Vars'] = array();
    $_SESSION['Session_Vars']['LoginTimeout'] = date("M j, Y H:i:s O", $timeout);
  }
    
  function clear() {
    foreach ($_SESSION['Session_Vars'] as $k => $v)
        unset($_SESSION['Session_Vars'][$k]);
    unset($_SESSION['Session_Vars']);
    $_SESSION = array();
    $_SESSION['Session_Vars'] = array();

    Trigger::Trigger('SessionCleared');
  }
	
	function __set($name, $value) {
		$_SESSION['Session_Vars'][$name] = $value;
	}
	
	function __get($name) {
		if (array_key_exists($name, $_SESSION['Session_Vars'])) {
			return $_SESSION['Session_Vars'][$name];
		}
	}
	
	function __unset($name) {
		if (array_key_exists($name, $_SESSION['Session_Vars'])) {
			unset($_SESSION['Session_Vars'][$name]);
		}
	}
	
	function __isset($name) {
		if (array_key_exists($name, $_SESSION['Session_Vars'])) return true;
		return false;
	}
	
	function store($name, $value=null) {
	
	}
}

?>