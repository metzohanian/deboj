<?php
namespace MetzOhanian\Deboj;

require_once ('common.php');
require_once ('Logger.php');
date_default_timezone_set(getenv('DEFAULT_TIMEZONE'));

class Application implements \Psr\Log\LoggerAwareInterface

{
  var $DefaultClass;
  var $DefaultCall;
  var $TemplateTheme;
  var $EXPOSED_ROUTES = array();
  static $PreInvoke;
  static $PostInvoke;
  static $PostInvokeError;
  var $Processor;
  var $ProcessCall;
  static $Logger;
  var $postsetup = null;
  var $cors = null;
  
  function __construct(\Psr\Log\LoggerInterface $logger = null) {
    Application::$Logger = $logger;
  }

  public function run() {
    Application::$Logger->info("Application::_bootstrap() phase", array(__FILE__, __LINE__, __CLASS__, __METHOD__));
    $this->_bootstrap();
    
    Application::$Logger->info("Application::configure() phase", array(__FILE__, __LINE__, __CLASS__, __METHOD__));
    $this->configure();
    
    Application::$Logger->info("Application::_appconfig() phase", array(__FILE__, __LINE__, __CLASS__, __METHOD__));
    $this->_appconfig();
    
    Application::$Logger->info("Application::startup() phase", array(__FILE__, __LINE__, __CLASS__, __METHOD__));
    $this->startup();
    
    Application::$Logger->info("Application::Process phase", array(__FILE__, __LINE__, __CLASS__, __METHOD__));
    call_user_func($this->ProcessCall, $this->Processor);
  }
  
  // https://stackoverflow.com/questions/2692332/require-all-files-in-a-folder
  function includeDir($path) {
    Application::$Logger->info("Application::includeDir()", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    $dir      = new \RecursiveDirectoryIterator($path);
    $iterator = new \RecursiveIteratorIterator($dir);
    foreach ($iterator as $file) {
      $fname = $file->getFilename();
      if (preg_match('%\.php$%', $fname)) {
        require_once($file->getPathname());
      }
    }
  }
  
  public function _bootstrap() {
    $this->includeDir(getenv('BOOTSTRAP'));
  }
  
  public function _appconfig() {
    $this->includeDir(getenv('APPCONFIG'));
  }
  
  public function setPostSetup($postsetup = null) {
    $this->postsetup = $postsetup;
  }
  
  public function setCors($cors = null) {
    $this->cors = $cors;
  }
  
	public function setLogger(\Psr\Log\LoggerInterface $logger) {
    Application::$Logger->info("Application::setLogger() - transfer to new Logger", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    Application::$Logger = & $logger;
		Lib::$Log = & $logger;	
    Application::$Logger->info("Application::setLogger() - new Logger set.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
	}
  
  public static function log($level, $message, $context = array()) {
    if (Lib::$Log) {
      if (method_exists(Lib::$Log, $level)) {
        Lib::$Log->$level($message, $context);
      }
    } else if ($this->Logger) {
      if (method_exists($this->Logger, $level)) {
        Application::$Logger->$level($message, $context);
      }
    }
  }
	
  function _require_dir_path($CONST, $path)
  {
    Application::$Logger->info("Application::_require_dir_path()", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    if (!file_exists($path)) {
      die('Configuration path ' . $CONST . ' at ' . $path . ' does not exist.');
      return Errors::SystemError('Configuration path ' . $CONST . ' at ' . $path . ' does not exist.');
    }
  }

  function configure()
  {
    if (version_compare(PHP_VERSION, "5.4.0", "<")) define('JSON_UNESCAPED_UNICODE', 0);
    
    if (strpos(__DIR__, getenv('APPROOT')) !== 0) {
      Application::$Logger->warning("APPROOT definition is different than the current working directory.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    }
    
    $this->_require_dir_path(getenv('SYSTEM'), getenv('SYSTEM'));
    $this->_require_dir_path(getenv('APPLIB'), getenv('APPLIB'));
    $this->_require_dir_path('MODEL', getenv('APPROOT') . '/model');
    $this->_require_dir_path('DRIVER', getenv('APPROOT') . '/model/driver');
    $this->_require_dir_path('LANGUAGE', getenv('APPROOT') . '/language');
    $this->_require_dir_path('BOOTSTRAP', getenv('APPROOT') . '/' . getenv('BOOTSTRAP'));
    $this->_require_dir_path('CONFIG', getenv('APPROOT') . '/' . getenv('APPCONFIG'));

    Application::$Logger->info("Application::configure() - register autoloader for framework.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    spl_autoload_register(function ($class) {
      $shortclass = substr(strrchr($class, "\\"), 1);
      $class_loaded = false;
      if (file_exists(getenv('APPLIB') . '/' . $shortclass . '.php')) {
        include_once (getenv('APPLIB') . '/' . $shortclass . '.php');
        $class_loaded = true;
      }
      if (file_exists(getenv('SYSTEM') . '/' . $shortclass . '.php')) {
        include_once (getenv('SYSTEM') . '/' . $shortclass . '.php');
        $class_loaded = true;
      }
      if (file_exists(getenv('APPROOT') . '/model/' . $shortclass . '.php')) {
        include_once (getenv('APPROOT') . '/model/' . $shortclass . '.php');
        $class_loaded = true;
      }
      if (file_exists(getenv('APPROOT') . '/language/' . $shortclass . '.php')) {
        include_once (getenv('APPROOT') . '/language/' . $shortclass . '.php');
        $class_loaded = true;
      }

      if (!$class_loaded) {
        Application::$Logger->error("Attempted to load a class that could not be found by the framework autoloader.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
      }
    });
    
    Application::$Logger->info("Standup SysLoader().", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    Lib::$Sys = new SysLoader();
    
    Application::$Logger->info("Standup \$Lib().", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    Lib::$Lib = new LibLoader(getenv('APPROOT') . '/lib', getenv('APP_NAMESPACE'));
    
    Application::$Logger->info("Standup \$Model().", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    Lib::$Model = new LibLoader(getenv('APPROOT') . '/model', getenv('APP_NAMESPACE') . "Model\\");
    
    Application::$Logger->info("Standup \$Language().", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    Lib::$Language = new LibLoader(getenv('APPROOT') . '/language', getenv('APP_NAMESPACE') . "Language\\");
    
    Application::$Logger->info("Standup \$Config().", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    Lib::$Config = new \stdClass();
    if (file_exists('config/config.php')) {
      require_once('config/config.php');
      foreach($CONFIG as $c => $config) Lib::$Config->$c = $config;
    }    
    
    Application::$Logger->info("Define Language.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args(), getenv('LANGUAGE')));
    Lib::$Sys->Session->Language = getenv('LANGUAGE');
  }

  public static function methodIsCallable($expose, $method_name = null) {
    Application::$Logger->info("Confirm API method is callable.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    
    return Lib::$Lib->methodIsCallable($expose, $method_name) || Lib::$Model->methodIsCallable($expose, $method_name);
  }
  
  public static function invokeMethod($expose, $method, $params) {
    Application::$Logger->info("Invoke API method.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    
    if (($parameters = Lib::$Lib->methodIsCallable($expose, $method)) !== false) {
      return Lib::$Lib->invokeMethod($expose, $method, $parameters, $params);
    }
    if (($parameters = Lib::$Model->methodIsCallable($expose, $method)) !== false) {
      return Lib::$Model->invokeMethod($expose, $method, $parameters, $params);
    }
    
    Application::$Logger->error("API method could not be called.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
  }
  
  function preInvoke($preInvoke) {
    LibLoader::$PreInvoke = $preInvoke; 
  }
  
  function postInvoke($postInvoke) {
    LibLoader::$PostInvoke = $postInvoke; 
  }
  
  function postInvokeError($postInvokeError) {
    Application::$PostInvokeError = $postInvokeError; 
  }
  
  function setProcessor($processor, $process) {
    Application::$Logger->info("Set transport and API resolution processor.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    
    $this->Processor = $processor;
    $this->ProcessCall = $process;
  }
  
  function setRepository($repo) {
    Application::$Logger->info("Set default repository.", array(__FILE__, __LINE__, __CLASS__, __METHOD__, func_get_args()));
    
    try {
      if (is_callable($repo)) {
        Lib::$Sys->Repository = $repo();
        return;
      }
      else if (is_object($repo)) {
        Lib::$Sys->Repository = $repo;
        return;
      }
    } catch (\Exception $e) {
      header('Content-Type: application/json');
      die('{"jsonrpc": "2.0", "error": {"code": -32099, "message": "Repository is not configured in .env file."}, "id": "1"}');
    }
    header('Content-Type: application/json');
    die('{"jsonrpc": "2.0", "error": {"code": -32099, "message": "Unrecognized repository type. Must be a closure or an object instance."}, "id": "1"}');
  }
  
  function startup()
  {
    Trigger::Trigger('LibDirsVerified');
    if (isset($this->postsetup)) call_user_func($this->postsetup);
    if (isset($this->cors)) call_user_func($this->cors);
  }
}


?>