<?php

namespace MetzOhanian\Deboj;

class LibLoader
{
  private $DRIVER;
  
  private $APPROOT;
  private $NAMESPACE;
  
  public $THUNK;
  
  private $decomposed;
  
  static $PreInvoke;
  static $PostInvoke;

  function __construct($APPROOT, $NAMESPACE)
  {
    $this->APPROOT = $APPROOT;
    $this->NAMESPACE = $NAMESPACE;
    $this->DRIVER = getenv('DB_DRIVER');
    $this->THUNK = new \stdClass();
  }
  
  private function validateParams($params) {
    $p = array();
    foreach ($this->decomposed['Order'] as $order => $param_name) {
      if (!array_key_exists($param_name, $params))
        if ($this->decomposed['Definition'][$param_name]['Optional'])
          $p[$order] = $this->decomposed['Definition'][$param_name]['DefaultValue'];
        else
          throw new \Exception("Required parameter `$param_name` not provided");
      else
        $p[$order] = $params[$param_name];
    }
    return $p;
  }
  
  public function decomposeMethod($class, $method = null) {
    $rm = is_null($method) ? new \ReflectionFunction($class) : new \ReflectionMethod($class, $method);
    if (!$rm->isPublic())
      return false;
    $decomp = array( 'Definition' => [], 'Required' => [], 'Order' => []);
    if ($rm) {
      $p = $rm->getParameters();
      foreach ($p as $k => $param) {
        $decomp['Definition'][$param->getName()] = array(
          'Optional' => $param->isOptional(),
          'DefaultValue' => $param->isOptional() ? $param->getDefaultValue() : null,
          'Order' => $param->getPosition(),
          'Type' => 'var',
          'UriName' => $param->getName(),
          'Name' => $param->getName()
        );
        $decomp['Order'][$param->getPosition()]  = $param->getName();
        if (!$decomp['Definition'][$param->getName()]['Optional'])
          array_push($decomp['Required'], $param->getName());
      }
      return $decomp;
    } else {
      return false;
    }
  }
  
  // check for method existence
  public function methodIsCallable($class, $method_name = null) {
    if (is_null($method_name)) {
      if (function_exists($class))
        return false;
      
      if (($method = $this->decomposeMethod($class, $method_name)) === false)
        return false;
      
      $this->decomposed = $method;
      
      return $method;
    }
    
    // Safely force class to load, even if called multiple times
    class_exists($this->NAMESPACE . $class);
    $load_class = false;
    if (method_exists($this->NAMESPACE . $class, $method_name)) {
      $load_class = $this->NAMESPACE . $class;
    }
    if (! $load_class) {
      return false; 
    }
    
    if (($method = $this->decomposeMethod($load_class, $method_name)) === false) {
      return false;
    }
    
    $this->decomposed = $method;
    
    if (!isset($this->$class))
      throw new \Exception("This endpoint {$class} @ $load_class is not available. ");        
      
    return $method;
  }
  
  // attempt to invoke the method with params
  public function invokeMethod($class, $method, $parameters, $params) {
    if (is_callable(LibLoader::$PreInvoke)) {
      list($class, $method, $params) = call_user_func(LibLoader::$PreInvoke, $class, $method, $parameters, $params);
    }
    // for named parameters, convert from object to assoc array
    // for no params, pass in empty array
    if ($params === null) {
      $params = array();
    } else if (is_assoc($params)) {
      $params = $this->validateParams($params);
    }
    
    $reflection = new \ReflectionMethod($this->NAMESPACE . $class, $method);
    
    // only allow calls to public functions
    if (!$reflection->isPublic() || substr($method, 0, 1) == '_') {
      throw new \Exception("Called method is not publicly accessible.");
    }
    
    // enforce correct number of arguments
    $num_required_params = $reflection->getNumberOfRequiredParameters();
    if ($num_required_params > count($params)) {
      throw new \Exception("Too few parameters passed.");
    }

    $invoke = null;
    ob_start();
      $invoke = $reflection->invokeArgs($this->$class, $params);
    ob_end_clean();
    
    if (is_callable(LibLoader::$PostInvoke)) {
      return call_user_func(LibLoader::$PostInvoke, $class, $method, $parameters, $params, $invoke);
    }
    return $invoke;
  }
  
  private function isset($class) {
    $classnamespace = $this->NAMESPACE;
    $fqdn = $classnamespace . $class;
    $classfile = "$class.php";
    SysLoader::force_include($this->APPROOT . "/" . $classfile);
    if (class_exists($fqdn)) {
      return array($fqdn, $classnamespace, $this->APPROOT);
    }
    
    return false;
  }
  
  public function __isset($class) {
    return $this->isset($class) ? true : false;
  }
  
  public function __get($class)
  {
    $loadProfile = $this->isset($class);
    if ($loadProfile) {
      $fqdn = $loadProfile[0];
      $classnamespace = $loadProfile[1];
      $classpath = $loadProfile[2];
      
      $this->$class = new $fqdn();
      
      if (file_exists($classpath . '/driver/' . $this->DRIVER . '.' . $class . '.php')) {
        SysLoader::force_include($classpath . '/driver/' . $this->DRIVER . '.' . $class . '.php');
        $driver_class = $classnamespace . "Driver\\" . $class;
        $this->$class->driver = new $driver_class();
      }
      
      return $this->$class;
    }
    
    throw new \Exception("Required file " . $this->APPROOT . "$classfile could not be found, or class $classname could not instantiated.");
  }

  public function LoadDriver($class, $driver)
  {
    if (isset($this->$class->$driver)) return;
    $driver_file = $this->DIR . '/driver/' . $driver . '.' . $class . '.php';
    if (file_exists($driver_file)) {
      SysLoader::force_include($driver_file);
      $driver_class = $driver . '_' . $class;
      $this->$class->$driver = new $driver_class();
    }
    else {
      throw new \Exception("Could not find $driver for $class at $driver_file.");
    }
  }
}

